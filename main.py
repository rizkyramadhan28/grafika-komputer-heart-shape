from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math
from math import sin, cos, tan, sqrt


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-500.0, 500.0, -500.0, 500.0)


def plotpoints():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1, 1.0, 1.0)
    glPointSize(13)
    glBegin(GL_LINES)
    glVertex2f(-500, 0)
    glVertex2f(500, 0)

    glVertex2f(0, -500)
    glVertex2f(0, 500)

    glEnd()
    heart_shape(0, 0, 20, 100)
    glFlush()


def heart_shape(x_center, y_center, r, n):
    glBegin(GL_POLYGON)

    # Rumus:
    # x = 16 * (sin t) ^ 3
    # y = 13 * cos t - 5 * cos 2t - 2 * cos 3t - cos 4t

    # 't' adalah sudut, di sini kita menyebutnya 'a'
    # Kalikan 'i' dengan 'a' agar sudut yang didapat tidak bersifat statis

    a = (2 * math.pi) / n
    i = 0

    while(i < n):
        x = x_center + r * (16 * (sin(i * a) ** 3))
        y = y_center + r * (13 * cos(i * a) - 5 * cos(2 * i * a) - 2 *
                            cos(3 * i * a) - cos(4 * i * a))

        glVertex2d(x, y)

        i += 1

    glEnd()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Heart Shape")
    glutDisplayFunc(plotpoints)

    init()
    glutMainLoop()


main()
