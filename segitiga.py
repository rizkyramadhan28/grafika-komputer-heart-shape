from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math


def init():
    glClearColor(0.0, 0.0, 0.0, 0.0)
    gluOrtho2D(-500.0, 500.0, -500.0, 500.0)


def plotpoints():
    glClear(GL_COLOR_BUFFER_BIT)
    glColor3f(1, 1.0, 1.0)
    glPointSize(13)
    glBegin(GL_LINES)
    glVertex2f(-500, 0)
    glVertex2f(500, 0)

    glVertex2f(0, -500)
    glVertex2f(0, 500)

    glEnd()
    segitiga(0, 0, 300, 3)
    glFlush()


def segitiga(x_center, y_center, r, n):
    glBegin(GL_POLYGON)

    # 'a' adalah sudut

    a = (2 * math.pi) / n
    i = 0

    while(i < n):
        # Mengkonversi koordinat sudut menjadi koordinat kartesius

        x = x_center + r * math.cos(i * a)
        y = y_center + r * math.sin(i * a)

        glVertex2d(x, y)

        i += 1
    glEnd()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    glutCreateWindow("Segitiga")
    glutDisplayFunc(plotpoints)

    init()
    glutMainLoop()


main()
